#!/bin/bash
# Écrire un script capable de vérifier si le nom d'utilisateur saisi correspond à un nom d'utilisateur déjà existant
echo "entrez un nom utilisateur"
read  nom

recherche=$(cut -d":" -f1 /etc/passwd | grep $nom)

if [ "$recherche" == "$nom" ]
then echo "le nom de l'utilisateur existe";
else echo "le nom de l'utilisateur n'existe pas";
fi
