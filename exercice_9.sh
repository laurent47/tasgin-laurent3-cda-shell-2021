#!/bin/bash
# Écrire un programme shell qui affiche tous les sous-répertoires du répertoire courant, en utilisant une boucle.

if [ "$1" = "" ]
then
	echo " vous avez pas rentrer argument réessaye"
        exit 1
fi

factorielle () {
	if (( "$1" <= 1 ))
	then
		echo 1
		return 0
	else
		echo $(($1*$(factorielle $(($1-1)))))
	fi
}
factorielle "$1"
echo "Est le  factorielle de $1"
